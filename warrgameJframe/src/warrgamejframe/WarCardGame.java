/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package warrgamejframe;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author avino
 */
public class WarCardGame extends javax.swing.JFrame {

    public int count;
    public ArrayList<Integer> pack = new ArrayList<>();
    public int size = 54;
    function f = new function();
    // int y;
    // int yy;

    public static void main() {

    }

    /**
     * Creates new form WarCardGame
     */
    public WarCardGame() {

        initComponents();
        pack.add(1);
        pack.add(2);
        pack.add(3);
        pack.add(4);
        pack.add(5);
        pack.add(6);
        pack.add(7);
        pack.add(8);
        pack.add(9);
        pack.add(10);
        pack.add(11);
        pack.add(12);
        pack.add(13);

        pack.add(1);
        pack.add(2);
        pack.add(3);
        pack.add(4);
        pack.add(5);
        pack.add(6);
        pack.add(7);
        pack.add(8);
        pack.add(9);
        pack.add(10);
        pack.add(11);
        pack.add(12);
        pack.add(13);

        pack.add(1);
        pack.add(2);
        pack.add(3);
        pack.add(4);
        pack.add(5);
        pack.add(6);
        pack.add(7);
        pack.add(8);
        pack.add(9);
        pack.add(10);
        pack.add(11);
        pack.add(12);
        pack.add(13);

        pack.add(1);
        pack.add(2);
        pack.add(3);
        pack.add(4);
        pack.add(5);
        pack.add(6);
        pack.add(7);
        pack.add(8);
        pack.add(9);
        pack.add(10);
        pack.add(11);
        pack.add(12);
        pack.add(13);

        pack.add(14);
        pack.add(14);

        pack();
        setLocationRelativeTo(null);

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton2 = new javax.swing.JButton();
        ButtomDeckOfCards = new javax.swing.JButton();
        ButtonCard2Player1 = new javax.swing.JButton();
        ButtonCard3Player1 = new javax.swing.JButton();
        LabelDesk2 = new javax.swing.JLabel();
        LabelPlayer1 = new javax.swing.JLabel();
        ButtonCard3Player2 = new javax.swing.JButton();
        ButtonCard2Player2 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        LabelTheWinnerIs = new javax.swing.JLabel();
        ButtonCard1Player2 = new javax.swing.JButton();
        ButtonCard1Player1 = new javax.swing.JButton();
        LabelDesk1 = new javax.swing.JLabel();
        ButtonTheWinnerIs = new javax.swing.JButton();
        LablePlayersWins = new javax.swing.JLabel();

        jButton2.setText("jButton2");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(null);

        ButtomDeckOfCards.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        ButtomDeckOfCards.setText("cards");
        ButtomDeckOfCards.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        ButtomDeckOfCards.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ButtomDeckOfCardsMouseClicked(evt);
            }
        });
        getContentPane().add(ButtomDeckOfCards);
        ButtomDeckOfCards.setBounds(830, 260, 144, 212);

        ButtonCard2Player1.setText("Card2");
        ButtonCard2Player1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ButtonCard2Player1MouseClicked(evt);
            }
        });
        getContentPane().add(ButtonCard2Player1);
        ButtonCard2Player1.setBounds(412, 537, 177, 179);

        ButtonCard3Player1.setText("Card3");
        ButtonCard3Player1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ButtonCard3Player1MouseClicked(evt);
            }
        });
        getContentPane().add(ButtonCard3Player1);
        ButtonCard3Player1.setBounds(662, 537, 177, 179);

        LabelDesk2.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        LabelDesk2.setText("      Desk2");
        getContentPane().add(LabelDesk2);
        LabelDesk2.setBounds(360, 300, 150, 77);

        LabelPlayer1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        LabelPlayer1.setText("            Player1");
        getContentPane().add(LabelPlayer1);
        LabelPlayer1.setBounds(857, 537, 150, 53);

        ButtonCard3Player2.setText("Card3");
        ButtonCard3Player2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ButtonCard3Player2MouseClicked(evt);
            }
        });
        getContentPane().add(ButtonCard3Player2);
        ButtonCard3Player2.setBounds(661, 10, 178, 190);

        ButtonCard2Player2.setText("Card2");
        ButtonCard2Player2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ButtonCard2Player2MouseClicked(evt);
            }
        });
        getContentPane().add(ButtonCard2Player2);
        ButtonCard2Player2.setBounds(401, 10, 177, 190);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setText("          Player2");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(857, 143, 150, 42);
        getContentPane().add(LabelTheWinnerIs);
        LabelTheWinnerIs.setBounds(47, 305, 138, 0);

        ButtonCard1Player2.setText("Card1");
        ButtonCard1Player2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ButtonCard1Player2MouseClicked(evt);
            }
        });
        getContentPane().add(ButtonCard1Player2);
        ButtonCard1Player2.setBounds(131, 10, 174, 190);

        ButtonCard1Player1.setText("Card1");
        ButtonCard1Player1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ButtonCard1Player1MouseClicked(evt);
            }
        });
        getContentPane().add(ButtonCard1Player1);
        ButtonCard1Player1.setBounds(134, 537, 174, 179);

        LabelDesk1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        LabelDesk1.setText("       Desk1");
        getContentPane().add(LabelDesk1);
        LabelDesk1.setBounds(370, 400, 150, 67);

        ButtonTheWinnerIs.setText("The Winner Is");
        ButtonTheWinnerIs.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ButtonTheWinnerIsMouseClicked(evt);
            }
        });
        getContentPane().add(ButtonTheWinnerIs);
        ButtonTheWinnerIs.setBounds(40, 310, 148, 53);
        getContentPane().add(LablePlayersWins);
        LablePlayersWins.setBounds(380, 370, 104, 37);

        pack();
    }// </editor-fold>//GEN-END:initComponents
        Random rand = new Random();


    private void ButtomDeckOfCardsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ButtomDeckOfCardsMouseClicked
        //yy = f.disapir(y);
//        System.out.println(yy);      
        count++;
        if (count >= 9) {
            ButtomDeckOfCards.setText("EMPTY");
            ButtomDeckOfCards.setEnabled(false);
        } else {
            Random r = new Random();
            int[] grades = new int[54];

            int inx;
            ButtonCard1Player1.setVisible(true);
            ButtonCard2Player1.setVisible(true);
            ButtonCard3Player1.setVisible(true);
            ButtonCard1Player2.setVisible(true);
            ButtonCard2Player2.setVisible(true);
            ButtonCard3Player2.setVisible(true);

            inx = rand.nextInt(size);
            System.out.println("" + pack.get(inx));
            ButtonCard1Player1.setText("" + pack.get(inx));
            size--;
            pack.remove(inx);

            inx = rand.nextInt(size);
            System.out.println("" + pack.get(inx));
            ButtonCard2Player1.setText("" + pack.get(inx));
            size--;
            pack.remove(inx);

            inx = rand.nextInt(size);
            System.out.println("" + pack.get(inx));
            ButtonCard3Player1.setText("" + pack.get(inx));
            size--;
            pack.remove(inx);

            inx = rand.nextInt(size);
            System.out.println("" + pack.get(inx));
            ButtonCard1Player2.setText("" + pack.get(inx));
            size--;
            pack.remove(inx);

            inx = rand.nextInt(size);
            System.out.println("" + pack.get(inx));
            ButtonCard2Player2.setText("" + pack.get(inx));
            size--;
            pack.remove(inx);

            inx = rand.nextInt(size);
            System.out.println("" + pack.get(inx));
            ButtonCard3Player2.setText("" + pack.get(inx));
            size--;
            pack.remove(inx);

            ButtomDeckOfCards.setVisible(false);
        }
    }//GEN-LAST:event_ButtomDeckOfCardsMouseClicked


    private void ButtonCard1Player2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ButtonCard1Player2MouseClicked

        ButtonCard2Player2.setVisible(false);
        ButtonCard3Player2.setVisible(false);

        LabelDesk2.setText("" + ButtonCard1Player2.getText());
        ButtonCard1Player2.setText("" + null);
        ButtonCard1Player2.setVisible(false);

    }//GEN-LAST:event_ButtonCard1Player2MouseClicked

    private void ButtonCard2Player2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ButtonCard2Player2MouseClicked

        ButtonCard1Player2.setVisible(false);
        ButtonCard3Player2.setVisible(false);

        LabelDesk2.setText("" + ButtonCard2Player2.getText());
        ButtonCard2Player2.setText("" + null);
        ButtonCard2Player2.setVisible(false);

    }//GEN-LAST:event_ButtonCard2Player2MouseClicked

    private void ButtonCard3Player2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ButtonCard3Player2MouseClicked

        ButtonCard1Player2.setVisible(false);
        ButtonCard2Player2.setVisible(false);

        LabelDesk2.setText("" + ButtonCard3Player2.getText());
        ButtonCard3Player2.setText("" + null);
        ButtonCard3Player2.setVisible(false);

    }//GEN-LAST:event_ButtonCard3Player2MouseClicked

    private void ButtonCard1Player1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ButtonCard1Player1MouseClicked

        ButtonCard2Player1.setVisible(false);
        ButtonCard3Player1.setVisible(false);

        LabelDesk1.setText("" + ButtonCard1Player1.getText());
        ButtonCard1Player1.setText("" + null);
        ButtonCard1Player1.setVisible(false);
    }//GEN-LAST:event_ButtonCard1Player1MouseClicked

    private void ButtonCard2Player1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ButtonCard2Player1MouseClicked

        ButtonCard1Player1.setVisible(false);
        ButtonCard3Player1.setVisible(false);

        LabelDesk1.setText("" + ButtonCard2Player1.getText());
        ButtonCard2Player1.setText("" + null);
        ButtonCard2Player1.setVisible(false);

    }//GEN-LAST:event_ButtonCard2Player1MouseClicked

    private void ButtonCard3Player1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ButtonCard3Player1MouseClicked

        ButtonCard1Player1.setVisible(false);
        ButtonCard2Player1.setVisible(false);

        LabelDesk1.setText("" + ButtonCard3Player1.getText());
        ButtonCard3Player1.setText("" + null);
        ButtonCard3Player1.setVisible(false);

    }//GEN-LAST:event_ButtonCard3Player1MouseClicked

    private void ButtonTheWinnerIsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ButtonTheWinnerIsMouseClicked
        LablePlayersWins.setText("");

        LablePlayersWins.setText(f.comper(LabelDesk1.getText(), LabelDesk2.getText()));
        LabelDesk1.setText("");
        LabelDesk2.setText("");

        if (ButtonCard1Player2.getText().equals("" + null) && ButtonCard2Player2.getText().equals("" + null)
                && ButtonCard3Player2.getText().equals("" + null) && ButtonCard1Player1.getText().equals("" + null)
                && ButtonCard2Player1.getText().equals("" + null) && ButtonCard3Player1.getText().equals("" + null)) {

            ButtomDeckOfCards.setVisible(true);

        }

        if (ButtonCard1Player2.getText().equals("" + null)) {
            ButtonCard1Player2.setVisible(false);
        } else {
            ButtonCard1Player2.setVisible(true);
        }
        if (ButtonCard2Player2.getText().equals("" + null)) {
            ButtonCard2Player2.setVisible(false);
        } else {
            ButtonCard2Player2.setVisible(true);
        }
        if (ButtonCard3Player2.getText().equals("" + null)) {
            ButtonCard3Player2.setVisible(false);
        } else {
            ButtonCard3Player2.setVisible(true);
        }
        if (ButtonCard1Player1.getText().equals("" + null)) {
            ButtonCard1Player1.setVisible(false);
        } else {
            ButtonCard1Player1.setVisible(true);
        }
        if (ButtonCard2Player1.getText().equals("" + null)) {
            ButtonCard2Player1.setVisible(false);
        } else {
            ButtonCard2Player1.setVisible(true);
        }
        if (ButtonCard3Player1.getText().equals("" + null)) {
            ButtonCard3Player1.setVisible(false);
        } else {
            ButtonCard3Player1.setVisible(true);
        }
    }//GEN-LAST:event_ButtonTheWinnerIsMouseClicked

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(WarCardGame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(WarCardGame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(WarCardGame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(WarCardGame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new WarCardGame().setVisible(true);
            }
        });
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton ButtomDeckOfCards;
    private javax.swing.JButton ButtonCard1Player1;
    private javax.swing.JButton ButtonCard1Player2;
    private javax.swing.JButton ButtonCard2Player1;
    private javax.swing.JButton ButtonCard2Player2;
    private javax.swing.JButton ButtonCard3Player1;
    private javax.swing.JButton ButtonCard3Player2;
    private javax.swing.JButton ButtonTheWinnerIs;
    private javax.swing.JLabel LabelDesk1;
    private javax.swing.JLabel LabelDesk2;
    private javax.swing.JLabel LabelPlayer1;
    private javax.swing.JLabel LabelTheWinnerIs;
    private javax.swing.JLabel LablePlayersWins;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    // End of variables declaration//GEN-END:variables

}
